# Guidance for Contributors

## Developer Dependencies

To start developing, `python3 -m venv .env && source .env/bin/activate`, `pip install -r requirements-dev.txt` and then `pre-commit install`.

## Adding a new Check

Follow these steps to add a new check.

1. Create a file `src/checks/<name_of_your_check_in_snake_case>.py`
2. Define the check-specific output format of your check in a JSON schema. Place the schema in the resource directory under `schemas/check_<NameOfYourCheckInUpperCamelCase>_output_format.json`.
    - The `results` key of your check's output will be validated against this schema.
3. Implement a `class <NameOfYourCheckInUpperCamelCase>(CheckInterface)` in the file created at step 1
    - The parent constructor will populate three instance attributes `repo`, `api`, and `proj` for you. The first is a `Repo` object representing a local checkout of the repository to check. The second is a `Gitlab` object giving you access to the API of the OpenCoDE Gitlab instance. The last is a `Project` object representing the remote project on the OpenCoDE GitLab.
    - If your check runs many different external tools, you might want to abstract the tools and their types behind common interfaces. Add those in a separate `src/checks/interfaces_<name_of_your_check_in_snake_case>.py` file.
    - The check MUST NOT change the state of the local or remote repository in any way.
    - Your check's `run` method MUST return the result of your parent's `run` method as well as a `score` key that hold a float between 0 and 1 inclusive. It MAY include a `results` key with check-specific results that match the schema created at step 2
    - If your check blows up in a way that you cannot recover from, raise a `CheckException`.
    - If you detect that you cannot perform your check on a repository (in a way that leads to a remotely meaningful result), raise a `CheckNotApplicableException`
    - Your check can store resources in the resource directory under `checks/<name_of_your_check_in_snake_case>/`. There are two types of resources. First, resources that are checked into the remote occmd repository. Those will be available on each run, in particular, those are the only resources available at the beginning of the first run. Second, generated resources that are not checked in to the remote repository. Those are created and managed by the application. All resources are preserved between runs.
4. If your check introduces a new dependency, add it to the `requirements.in` file. Then run `pip-compile requirements.in` to re-generate the `requirements.txt` file and `pip-sync` to update your virtual environment to reflect the changes.
5. Optional: Create your checks resource directory and populate it with checked-in resources.
    - You might want to use git submodules for easier updating of existing installations, e.g., by (optionally) pulling at the beginning of a run.
    - The resources must be read-only at runtime.
    - You have to add exceptions for these files to the `.dockerignore` and `.gitignore` files.
6. Import your check class (the one that implements the `CheckInterface`) at the top if `src/checks/__init__.py` and add it to the `available_checks` variable at the top of the file.
7. Implement one or two tests for your new check. Have a look at the `tests` directory for inspiration.

That's it, congratulations, your check is now included in the occmd tool! It should show up in the output of `occmd check --help` under the possible values of the `-c` flag. The return value of your check will be written to stdout as a JSON object.
