_NAME = occmd
_BRANCH = $(shell git rev-parse --abbrev-ref HEAD)
_COMMIT = $(shell git rev-list HEAD -n 1)
_TAG = $(_NAME)_$(_BRANCH)_$(_COMMIT)

.PHONY: test build run

test:
	pytest tests

occmdcfg.ini:
	cp examples/occmdcfg.ini .

test-ci: occmdcfg.ini
	pytest -s -v tests

build:
	docker build -t $(_TAG) .

run:
	docker run 							\
		--rm 							\
		-e OC_GL_APIKEY=$(OC_GL_APIKEY) 			\
		--mount "type=volume,src=res_$(_TAG),dst=/opt/occmd/resources" \
		--mount "type=bind,src=$(OCCMD_TARGET),dst=/opt/target"	\
		$(_TAG)							\
		-i $(OCCMD_ID) $(OCCMD_ARGS)
