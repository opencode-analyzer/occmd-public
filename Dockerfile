FROM python:3.11-slim

# Keeps Python from generating .pyc files in the container
ENV PYTHONDONTWRITEBYTECODE=1

# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED=1

ENV DEBIAN_FRONTEND=noninteractive

# Install system requirements
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        file \
        git \
        jq \
        wget \
    && rm -rf /var/cache/apt/archives /var/lib/apt/lists

# @tokei: count loc
WORKDIR /bin
SHELL ["/bin/bash", "-o", "pipefail", "-c"]
RUN wget -q \
    https://github.com/XAMPPRocky/tokei/releases/download/v13.0.0-alpha.0/tokei-x86_64-unknown-linux-musl.tar.gz \
    -O - | \
    tar zxf -

RUN mkdir -p /opt/target /opt/occmd

# Creates a non-root user with an explicit UID and adds permission to access
# the relevant directories.
RUN adduser -u 5678 --disabled-password --gecos "" ocuser && \
    mkdir -p /opt/occmd && \
    chown -R ocuser:ocuser /opt/target /opt/occmd
USER ocuser

WORKDIR /opt/occmd

# Install pip requirements
COPY requirements.txt .
RUN python -m pip install --no-cache-dir -r requirements.txt

# copy files
COPY --chown=ocuser occmd examples/occmdcfg.ini ./
COPY --chown=ocuser src src
COPY --chown=ocuser resources resources

# Prepare config file
RUN sed \
    -i \
    's/resources_dir =.*/resources_dir = \/opt\/occmd\/resources/' \
    occmdcfg.ini

# mark mounted directory as safe to avoid "dubious ownership" error
RUN git config --global --add safe.directory /opt/target

# Persist resources
VOLUME /opt/occmd/resources

# Limit container to check subcommand, expect project-to-check to be mounted at
# /opt/target. User can select check and must supply project ID.
ENTRYPOINT ["/opt/occmd/occmd", "check", "-d", "/opt/target"]
