import subprocess as sp

from src.constants import OCCMD_VERSION_STRING


class TestE2eCliHelp:
    def test_run_base(self) -> None:
        sp.run(["./occmd", "--help"], check=True)

        proc = sp.run(["./occmd", "--version"], check=True, capture_output=True)
        assert proc.stdout.decode().strip() == OCCMD_VERSION_STRING

    def tests_run_sub(self) -> None:
        for sub in ["check", "update", "dashboard"]:
            sp.run(["./occmd", sub, "--help"], check=True)
