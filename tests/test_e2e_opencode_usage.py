from typing import TYPE_CHECKING, Any

from src.checks.opencode_usage import OpencodeUsage
from src.constants import CHECK_MAX_SCORE, CHECK_MIN_SCORE
from tests.util_testing import get_check_instance_by_id

if TYPE_CHECKING:
    from src.interfaces import CheckInterface

TEST_REPO_ID: int = 1871


def test_opencode_usage() -> None:
    check: CheckInterface = get_check_instance_by_id(
        TEST_REPO_ID, OpencodeUsage
    )

    result: dict[str, Any] = check.run()

    assert result.get("score") == CHECK_MIN_SCORE
    assert "opencode_usage" in result["results"]

    heuristics = {
        h["name"]: h["value"] for h in result["results"]["opencode_usage"]
    }
    assert heuristics["readme"] == CHECK_MAX_SCORE
    assert heuristics["description"] == CHECK_MIN_SCORE
    assert heuristics["merge requests"] == CHECK_MAX_SCORE
    assert heuristics["publiccode.yml"] == CHECK_MAX_SCORE
    assert heuristics[".github folder"] == CHECK_MAX_SCORE
    assert CHECK_MIN_SCORE < heuristics["committers"] < CHECK_MAX_SCORE
