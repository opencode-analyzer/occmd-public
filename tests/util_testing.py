from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from git.repo import Repo
    from gitlab.v4.objects import Project

from src.interfaces import CheckInterface
from src.opencode import OpenCode


def get_check_instance_by_id(
    _id: int, check_class: type[CheckInterface]
) -> CheckInterface:
    oc: OpenCode = OpenCode()
    proj: Project = oc.get_project_by_id(_id)
    repo: Repo | None = None
    try:
        # if available, local dump is much faster
        repo = oc.get_repo_for_proj(proj)
    except:  # noqa: E722
        # fall back to cloning
        repo = oc.clone_repo_for_proj(proj)
    check: CheckInterface = check_class(proj, repo, api=oc.gl)

    return check
