from typing import TYPE_CHECKING, Any

from src.checks.security_policy import SecurityPolicy
from src.constants import CHECK_GOOD_SCORE, CHECK_MAX_SCORE
from tests.util_testing import get_check_instance_by_id

if TYPE_CHECKING:
    from src.interfaces import CheckInterface

TEST_REPO_ID: int = 2154


def test_opencode_usage() -> None:
    check: CheckInterface = get_check_instance_by_id(
        TEST_REPO_ID, SecurityPolicy
    )

    result: dict[str, Any] = check.run()

    assert CHECK_GOOD_SCORE < float(result.get("score", 0.0)) < CHECK_MAX_SCORE
    assert "security_policy" in result["results"]

    findings = {
        h["path"]: h["findings"] for h in result["results"]["security_policy"]
    }

    assert len(findings) == 2  # noqa: PLR2004
    assert "Security.md" in findings
    assert len(findings["Security.md"]) == 3  # noqa: PLR2004
    assert "docs/security.rst" in findings
    assert len(findings["docs/security.rst"]) == 2  # noqa: PLR2004
