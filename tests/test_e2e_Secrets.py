from typing import TYPE_CHECKING, Any

from src.checks.secrets import Secrets
from src.constants import CHECK_BAD_SCORE, CHECK_MAX_SCORE
from tests.util_testing import get_check_instance_by_id

if TYPE_CHECKING:
    from src.interfaces import CheckInterface


class TestE2eSecrets:
    SECRETS_BAD_01: int = 1187
    SECRETS_GOOD_01: int = 1188

    def test_bad_01(self) -> None:
        check: CheckInterface = get_check_instance_by_id(
            self.SECRETS_BAD_01, Secrets
        )

        result: dict[str, Any] = check.run(args_dict={"baseline": 1})

        assert result.get("score", 1.0) < CHECK_BAD_SCORE

    def test_good_01(self) -> None:
        check: CheckInterface = get_check_instance_by_id(
            self.SECRETS_GOOD_01, Secrets
        )

        result: dict[str, Any] = check.run(args_dict={"baseline": 1})

        assert result.get("score") == CHECK_MAX_SCORE
