from typing import TYPE_CHECKING, Any

from src.checks.existence_of_documentation_infrastructure import (
    ExistenceOfDocumentationInfrastructure,
)
from tests.util_testing import get_check_instance_by_id

if TYPE_CHECKING:
    from src.interfaces import CheckInterface


class TestE2eExistenceOfDocumentationInfrastructure:
    EXISTENCE_OF_DOCUMENTATION_INFRASTRUCTURE_GOOD_01 = 1768

    def test_good_01(self):
        check: CheckInterface = get_check_instance_by_id(
            self.EXISTENCE_OF_DOCUMENTATION_INFRASTRUCTURE_GOOD_01,
            ExistenceOfDocumentationInfrastructure,
        )

        result: dict[str, Any] = check.run(args_dict=None)

        assert result == {
            "score": 0.7499999749678717,
            "results": {
                "documentation_type_information": [
                    {"name": "OutOfTreeWiki", "confidence": 1.0, "amount": 12},
                    {
                        "name": "OutOfTreeExternal",
                        "confidence": 1.0,
                        "amount": 1001999,
                    },
                    {
                        "name": "PlainInTreeFile",
                        "confidence": 1.0,
                        "amount": 651,
                    },
                    {
                        "name": "PlainInTreeFolder",
                        "confidence": 0.77,
                        "amount": 27,
                    },
                ]
            },
            "id": 1768,
            "check": "ExistenceOfDocumentationInfrastructure",
        }
