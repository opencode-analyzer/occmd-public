from typing import TYPE_CHECKING, Any

from src.checks.signed_releases import SignedReleases

if TYPE_CHECKING:
    from src.interfaces import CheckInterface

from tests.util_testing import get_check_instance_by_id

SIGNED_RELEASES_GOOD_01: int = 2149
SIGNED_RELEASES_BAD_01: int = 2155


class TestE2eSignedReleases:
    def test_bad_01(self) -> None:
        check: CheckInterface = get_check_instance_by_id(
            SIGNED_RELEASES_BAD_01, SignedReleases
        )

        result: dict[str, Any] = check.run(args_dict=None)

        assert result == {
            "id": 2155,
            "check": "SignedReleases",
            "score": 0.0,
            "results": {
                "release_info": [
                    {
                        "files": ["a"],
                        "score": 0,
                        "tag": "v0.0.1",
                        "timestamp": "2023-11-27T14:45:13.048Z",
                    }
                ]
            },
        }

    def test_good_01(self) -> None:
        check: CheckInterface = get_check_instance_by_id(
            SIGNED_RELEASES_GOOD_01, SignedReleases
        )

        result: dict[str, Any] = check.run(args_dict=None)

        assert result == {
            "id": 2149,
            "check": "SignedReleases",
            "score": 0.8,
            "results": {
                "release_info": [
                    {
                        "files": ["a.sha521sum", "a", "a.sha521sum.asc"],
                        "score": 8,
                        "tag": "v0.1",
                        "timestamp": "2023-11-27T08:54:18.199Z",
                    }
                ]
            },
        }
