from typing import TYPE_CHECKING, Any

from src.checks.checked_in_binaries import CheckedInBinaries
from src.constants import CHECK_MAX_SCORE, CHECK_MIN_SCORE
from tests.util_testing import get_check_instance_by_id

if TYPE_CHECKING:
    from src.interfaces import CheckInterface


class TestE2eCheckedInBinaries:
    CHECKED_IN_BINARIES_BAD_01 = 1048
    CHECKED_IN_BINARIES_GOOD_01 = 1049

    def test_bad_01(self) -> None:
        check: CheckInterface = get_check_instance_by_id(
            self.CHECKED_IN_BINARIES_BAD_01, CheckedInBinaries
        )

        result: dict[str, Any] = check.run(args_dict=None)

        assert result.get("score") == CHECK_MIN_SCORE

    def test_good_01(self):
        check: CheckInterface = get_check_instance_by_id(
            self.CHECKED_IN_BINARIES_GOOD_01, CheckedInBinaries
        )

        result: dict[str, Any] = check.run(args_dict=None)

        assert result.get("score") == CHECK_MAX_SCORE
