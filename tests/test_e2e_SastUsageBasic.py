from typing import TYPE_CHECKING, Any

from src.checks.sast_usage_basic import SastUsageBasic
from tests.util_testing import get_check_instance_by_id

if TYPE_CHECKING:
    from src.interfaces import CheckInterface

SUB_GOOD_01: int = 2235


class TestE2eExistenceOfDocumentationInfrastructure:
    def test_good_01(self) -> None:
        check: CheckInterface = get_check_instance_by_id(
            SUB_GOOD_01,
            SastUsageBasic,
        )

        result: dict[str, Any] = check.run(args_dict=None)

        for tool in ["pyright", "mypy", "pylint", "ruff"]:
            assert tool in result["results"]["lang_tools"][0][1]
