# occmd

The **O**pen**C**ode **C**o**M**man**D** is a tool for scanning an OpenCoDE project's repository.
It consists of several analysis checks. This document provides a high-level overview of the tool.
Implementation details are generated with [Sphinx Doc](https://www.sphinx-doc.org/en/master/index.html) and are accessible [here](https://opencode-analyzer.usercontent.opencode.de/-/occmd-public/-/jobs/338306/artifacts/public/index.html).

## Architecture

This section describes how the occmd tool works. Figure 1 shows the architecture starting from the tool platform execution until it outputs the result.
Please refer to the OpenCoDE system architecture documentation for a broader perspective.

![Architecture Overview](img/occmd_architecture.png)
*Figure 1: Architecture Overview*

### How it works

The occmd tool requires the occmdcfg.ini file for configuring the resource folders and persistent tool results.
The values of this config file can only be changed during image building.
When starting the tool, it accepts multiple arguments but requires the project ID to run the checks.
The tool then verifies the project ID through the GitLab API.
To access private repositories, it requires a GitLab token and username which is set via environment variables.
Once the project ID is verified to be accessible, the checks runs one after another.
If no specific checks are explicitly passed as an argument, it runs every available checks.
For each successful checks, results are appended one after another. The results are then printed as a JSON string.

### Current implementation

In production, the tool runs inside the platform pipeline. On dev environment, the occmd tool runs inside the data-provider to ease debugging.
The occmd and its dependencies are locally installed inside data-provider.
Whenever the data-provider evaluates a project, it opens up a process that runs a script for running the occmd tool.
The script requires a project ID and the local path to the cloned project.
The GitLab token & username needed for accessing private repos are currently loaded through the data-provider's env variables.
After running the script, the data-provider then parses the standard output of the process into JSON.
At this point, the JSON result is treated as any other tool result.
In short, instead of running the occmd tool in the pipeline, currently occmd runs inside the data-provider.

## Checks

### Existing Checks

This section gives a general description of currently implemented check.

<details><summary>

#### Checked-in-Binaries

</summary>

This check scans for executables, which are the byproduct of program compilation.
Such files could be harmful for multiple reasons i.e., it is difficult to check their content & against the version control system's best practices.
This check works by determining the file type for every file in the project and compares it to a blacklist of binary executable file formats.
The binary blacklist is saved as its own [repository](https://gitlab.opencode.de/opencode-analyzer/occmd-checked_in_binaries-blacklist) and can be updated as needed by the maintainer.
The final score is either 0 or 1.
</details>

<details><summary>

#### Comments in Code

</summary>

This check uses [Tokei program](https://github.com/XAMPPRocky/tokei) to count the number of lines of codes and comments.
Especially with the mainstream use of automated [documentation generators](https://en.wikipedia.org/wiki/Comparison_of_documentation_generators),
writing comments in source code reflects how well a program is documented. The check builds the ratio between actual code and comments.
A perfect score of 1 could however be achieved even with a low comment-to-code ratio.
</details>

<details><summary>

#### Existence of Documentation Infrastructure

</summary>

The check performs heuristics to estimate the amount of existing documentation for the software developed in a repository.
It scans the repository for files, folders, and external documentation references such as Wiki sites or GitLab Project Site.
For searching documentation-related files and folders, it uses a whitelist. For external references, it searches for links in any text files.
The final score depends on the total identified documentation types and ranges from 0 to 1.
</details>

<details><summary>

#### OpenCoDE Usage

</summary>

This check determines whether a repository is developed actively on OpenCoDE or if the project is just a mirror.
An active development on OpenCoDE means that the project is also being evaluated by the OpenCoDE Analyzer.
It verifies commit authors, looks for the existence of publiccode.yml, reads project descriptions, etc.
Some methods give a clear answer regarding OpenCoDE usage, but others are not.
Therefore the check runs heuristics against the results to determine whether it is a mirror. The final score is either 0 or 1.
</details>

<details><summary>

#### SAST Usage

</summary>

This check verifies the existence of SAST tools by looking for any references and directives.
It then checks whether the detected tools are also used in the project, such as through git hooks or other config files.
It matches the programming languages used in the project with their respective tools.
The score calculation depends on whether each programming language in the project uses at least one adequate analysis tool.
The final score is from 0 to 1 and represents the weighted ratio of "SAST using" programming languages in the project.
</details>

<details><summary>

#### Secrets

</summary>

This check looks for leaked secrets in a git repository. It uses the [detect-secrets](https://github.com/Yelp/detect-secrets) for backward-compatible secret detection.
It creates a baseline of potential secrets found in the project and builds a diff in future secret scans.
It also detects secrets in the git history to ensure that developers also delete previously pushed secrets.
In cases where the check produces false positives, the project owner may adjust the configuration of this check.
The final score ranges from 0 to 1.
</details>

<details><summary>

#### Security Policy

</summary>

This check determines whether there is a security policy file and checks the content.
It looks for specific paths and extensions and scans the content for relevant keywords such as links, emails, and other info.
The final score ranges from 0 to 1.
</details>

<details><summary>

#### Signed Release

</summary>

This check verifies that non-default artifacts in releases are cryptographically signed. It assumes that an attacker has gained full write access to a repository.
Thus, artifacts and releases being generated in CI are not sufficient to prove their integrity.
Artifacts have to be signed with a private key that is kept separate from the repository. Signatures must be uploaded to the release.
The check looks for specific signature extensions and analyzes up to **N** previous releases.
The calculation is based on the average of the last **N** signed releases. The final score ranges from 0 to 1.
</details>

### Implementing New Checks

This section briefly explains how to add new checks. For a more detailed version, please refer to the [CONTRIBUTING.md](../CONTRIBUTING.md). It is assumed you have set up occmd locally. The following provides a brief checklist to extend the occmd tool:

- [ ] Add any required dependency in `requirements.in`, also in `requirements-dev.in` for any dev-specific packages.
- [ ] Generate new requirements.txt using `pip-compile --output-file=requirements.txt requirements.in`.
- [ ] For dependencies outside of the Python environment, please modify the `Dockerfile` to install the package.
- [ ] Write your new check in a Python file and put it in `src/checks/`.
- [ ] Add your class to the `available_checks` list in `src/checks/__init__.py`.
- [ ] Implement your check as a class that inherits `CheckInterface`.
- [ ] Provide a `run` method that calls the check's functionality and returns the result.
- [ ] The result must be a dictionary with a content: `{'score': float, 'result': dict[str, Any]}`.
- [ ] Make the check result schemas in `resources/schemas`.
- [ ] Write unit tests for your new check in the `tests/` folder.
- [ ] Write a brief description of what your check does in this document.
