"""
Exceptions that might be raised by core or check code.
"""


class CoreGoesBoomError(Exception):
    """
    Raised if core code encounters an unrecoverable error (uncaught).
    """


"""
Exceptions that might be raised by check code and must be caught by
core code.
"""


class CheckGoesBoomError(Exception):
    """
    Raise this exception if your check exploded. (at runtime)
    """


class CheckConstructionError(Exception):
    """
    Raise this exception if your check's constructor encountered some fatal
    error.
    """


class CheckNotApplicableError(Exception):
    """
    Raise this exception if your check comes to the conclusion that it cannot
    produce a reasonable result for the griven project. (at runtime)
    """
