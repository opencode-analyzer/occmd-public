"""
Implementation of the security policy check, which attempts to determine
whether there is a security policy file (usually security.md) and checks
the content.
"""
import re
from enum import Enum
from itertools import product
from typing import Any

from pydantic import BaseModel, ConfigDict

from src.interfaces import CheckInterface
from src.utils import file_list

POLICY_PATHS = ["", "doc/", "docs/", "docsrc/", "src/", "src/doc/"]
POLICY_SUFFIXES = ["adoc", "md", "markdown", "rst"]
SECURITY_POLICY_PATHS = {
    f"{path}security.{suffix}"
    for path, suffix in product(POLICY_PATHS, POLICY_SUFFIXES)
}

VULN_INFO_REGEX = re.compile(
    r"(Disclos|Vuln|Security|Bug|Sicherheit|Melde|Schwachstelle)",
    flags=re.IGNORECASE,
)
EMAIL_REGEX = re.compile(
    r"\b([a-z0-9_.+-]+(?:@|\(at\))[a-z0-9-]+\.[a-z0-9-.]+|e-?mail)\b",
    flags=re.IGNORECASE,
)
URL_REGEX = re.compile(r"https?://[a-zA-Z0-9./?=_%:-]*")
DEFAULT_OFFSET = 1
MINIMUM_EXPECTED_POLICY_TEXT = 100


class FindingType(str, Enum):
    email = "email"
    link = "link"
    vuln_info = "vuln_info"


CONTACT_INFORMATION_TYPES = {FindingType.email, FindingType.link}


class SecurityPolicyFinding(BaseModel):
    # config: convert enums to strings automatically
    model_config = ConfigDict(use_enum_values=True)

    finding_type: FindingType
    value: str
    line_number: int
    offset: int


class SecurityPolicyFile(BaseModel):
    path: str  # path as str relative to repo root
    findings: list[SecurityPolicyFinding]

    # this is a private attribute and will not be included in the JSON result
    _content = None

    def __init__(self, content: str, **kwargs: Any) -> None:
        super().__init__(**kwargs)
        self._content = content

    @property
    def content(self):
        return self._content


class SecurityPolicyResults(BaseModel):
    security_policy: list[SecurityPolicyFile]


class SecurityPolicy(CheckInterface):
    def run(self, args_dict: dict[str, Any] | None = None) -> dict[str, Any]:
        result: dict[str, Any] = super().run(args_dict)
        policy_data = self._get_security_policy_results()
        return {
            "score": _compute_score(policy_data),
            "results": SecurityPolicyResults(
                security_policy=policy_data
            ).model_dump(),
        } | result

    def _get_security_policy_results(self) -> list[SecurityPolicyFile]:
        results = []
        for file in file_list(self.repo):
            relative_path = str(file.relative_to(self.repo.working_dir))
            if not _is_security_policy(relative_path):
                continue
            content = file.read_text(errors="ignore")
            results.append(
                SecurityPolicyFile(
                    path=relative_path,
                    findings=_check_policy_content(content),
                    content=content,
                )
            )
        return results


def _is_security_policy(path: str) -> bool:
    return path.lower() in SECURITY_POLICY_PATHS


def _check_policy_content(content: str) -> list[SecurityPolicyFinding]:
    if not content:
        return []

    findings = []
    for line_num, line in enumerate(content.splitlines()):
        if not line:
            continue
        for regex, type_ in [
            (URL_REGEX, FindingType.link),
            (EMAIL_REGEX, FindingType.email),
            (VULN_INFO_REGEX, FindingType.vuln_info),
        ]:
            for match in regex.finditer(line):
                findings.append(
                    SecurityPolicyFinding(
                        finding_type=type_,
                        value=match.group(),
                        line_number=line_num,
                        offset=match.start(),
                    )
                )
    return findings


def _score_policy_exists(_: SecurityPolicyFile) -> float:
    # dummy function to check if a policy file exists
    return 1.0


def _score_policy_links(policy: SecurityPolicyFile) -> float:
    # check if there are any links or email addresses
    if any(
        f.finding_type in CONTACT_INFORMATION_TYPES for f in policy.findings
    ):
        return 1.0
    return 0.0


def _score_policy_text(policy: SecurityPolicyFile) -> float:
    # check if there is text (besides found links/emails) in the security policy
    linked_content = sum(
        len(f.value)
        for f in policy.findings
        if f.finding_type in CONTACT_INFORMATION_TYPES
    )
    effective_content = max(len(policy.content) - linked_content, 0)
    return (
        min(effective_content, MINIMUM_EXPECTED_POLICY_TEXT)
        / MINIMUM_EXPECTED_POLICY_TEXT
    )


def _score_vuln_disclosure(policy: SecurityPolicyFile) -> float:
    # check if there is text about disclosure of vulnerabilities
    if any(f.finding_type == FindingType.vuln_info for f in policy.findings):
        return 1.0
    return 0.0


SCORING_METRICS = [
    (_score_policy_exists, 2),
    (_score_policy_links, 5),
    (_score_policy_text, 3),
    (_score_vuln_disclosure, 5),
]


def _compute_score(policies: list[SecurityPolicyFile]) -> float:
    total = sum(weight for _, weight in SCORING_METRICS)
    score = 0.0

    if policies:
        for scoring_function, weight in SCORING_METRICS:
            score += max(scoring_function(p) for p in policies) * weight / total

    return score
