"""
Checks that non-default artifacts in releases are cryptographically signed

The threat model assumes that an attacker has gained full write acces to a
repository (code, package/container registry, automations, ...). Thus,
artifacts and releases being generated in CI is not sufficient to prove their
integrity. Artifacts have to be downloaded and signed with a private key that
is kept separate from the repository access credentials, singatures must then be
uploaded to the release.
"""
import logging
from typing import Any, Optional, cast
from urllib.error import URLError
from urllib.request import urlopen

from gitlab.exceptions import GitlabAuthenticationError, GitlabListError
from gitlab.v4.objects import ProjectRelease

from src.exceptions import CheckGoesBoomError, CheckNotApplicableError
from src.interfaces import CheckInterface

logger: logging.Logger = logging.getLogger(__name__)


# file extensions that indicate that the file contains a cryptographic
# signature
SIGNATURE_EXTENSIONS: list[str] = [".asc", ".minisig", ".sig", ".sign"]
SIGNATURE_SCORE: int = 8
# possible file extensions of SLSA provenance files
PROVENANCE_EXTENSIONS: list[str] = [".intoto.jsonl"]
PROVENANCE_SCORE: int = 10
# how many releases we analyze
RELEASE_LOOK_BACK: int = 5


class SignedReleases(CheckInterface):
    def get_releases(self) -> list[ProjectRelease]:
        """Get all releases that include non-standard assets"""
        releases: list[ProjectRelease] = []

        try:
            releases = cast(
                list[ProjectRelease], self.proj.releases.list(get_all=True)
            )
        except (GitlabAuthenticationError, GitlabListError) as E:
            logger.info(f"Cannot fetch releases: {E}")

        logger.info(f"Project has {len(releases)} releases")

        # Skip releases that have no assets attached (besides the compressed
        # source code that is always present)
        return [r for r in releases if r.assets["links"]]

    def extract_file_name(self, url: str) -> str:
        """Takes a file download url and extracts the file name"""
        filename: str = ""

        try:
            resp = urlopen(url)
        except URLError as E:
            logger.info(f"Unable to open URL {url}: {E}")
            return filename

        # we expect that file downloads use the Content Disposition header to
        # suggest a file name for the downloaded file according to RFC 2183
        content_disposition: list[str] = [
            x.strip()
            for x in resp.headers.get("Content-Disposition", "").split(";")
        ]

        logger.info(f"Content-Disposition: {content_disposition}")

        # we expect at least one disposition parameter
        if len(content_disposition) < 2:  # # noqa: PLR2004
            logger.info("No disposition parameters")
            return filename

        content_type: str = content_disposition[0].lower()
        if content_type != "attachment":
            logging.info(f"Not an attachment: {content_type}")
            return filename

        disposition_param: list[str] = content_disposition[1:]
        filename_param_prefix: str = "filename="
        for param in disposition_param:
            if not param.startswith(filename_param_prefix):
                continue
            filename = param[len(filename_param_prefix) :]
            break

        return filename.replace('"', "")

    def _ext_score(self, ext: str, files: list[str], score: int) -> int:
        for file in files:
            if file.endswith(ext):
                return score

        return 0

    def score_release(
        self, r: ProjectRelease
    ) -> tuple[int, dict[str, int | list[str] | str]]:
        """
        Calculates the score of a single release with non-standard files.

        :param r: release to score
        :return: score, detailed breakdown of the score
        """
        files: list[str] = [
            self.extract_file_name(str(link["url"]))
            for link in r.assets["links"]
        ]

        logger.info(f"Release {r.tag_name} contains files: {files}")

        if not files:
            raise CheckGoesBoomError(
                "BUG: attempt to score a release without non-standard assets"
            )

        # filter out cases where we were unable to retreive the filename
        files = [file for file in files if file]

        # try provenance first
        score: int = 0
        for ext in PROVENANCE_EXTENSIONS:
            score = self._ext_score(ext, files, PROVENANCE_SCORE)
            if score != 0:
                break

        # we did not find any provenance files, try signatures
        if score == 0:
            for ext in SIGNATURE_EXTENSIONS:
                score = self._ext_score(ext, files, SIGNATURE_SCORE)
                if score != 0:
                    break

        return score, {
            "files": files,
            "score": score,
            "tag": r.tag_name,
            "timestamp": r.created_at,
        }

    def run(self, args_dict: Optional[dict[str, Any]] = None) -> dict[str, Any]:
        ret: dict[str, Any] = super().run(args_dict)

        releases: list[ProjectRelease] = self.get_releases()
        if not releases:
            raise CheckNotApplicableError(
                "Project has no releases with non-standard assets"
            )

        logger.info(
            f"Project has {len(releases)} releases with non-standard assets"
        )

        # we consider up to the N most recent releases with non-standard
        # assets
        releases = releases[:RELEASE_LOOK_BACK]

        score: float = 0
        release_info: list[dict[str, int | list[str] | str]] = []
        for r in releases:
            release_score, score_details = self.score_release(r)

            score += release_score
            release_info.append(score_details)

        score /= 10 * len(releases)

        return ret | {
            "score": score,
            "results": {"release_info": release_info},
        }
